package com.example.rostrzep.countriesinterview.countires.country_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rostrzep.countriesinterview.BaseActivity;
import com.example.rostrzep.countriesinterview.R;
import com.example.rostrzep.countriesinterview.dagger.ActivityModule;
import com.example.rostrzep.countriesinterview.dagger.ActivityScope;
import com.example.rostrzep.countriesinterview.dagger.AppComponent;
import com.example.rostrzep.countriesinterview.model.CountryItem;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class CountryDetailsActivity extends BaseActivity {

    public static final String EXTRA_COUNTRY_ITEM = "extra_country_item";

    public static Intent newIntent(final Context context, final CountryItem countryItem) {
        final Intent intent = new Intent(context, CountryDetailsActivity.class);
        intent.putExtra(EXTRA_COUNTRY_ITEM, countryItem);
        return intent;
    }

    @BindView(R.id.country_name)
    TextView countryName;
    @BindView(R.id.country_avatar)
    ImageView countryAvatar;
    @BindView(R.id.country_people_amount)
    TextView countryPeopleAmount;
    @BindView(R.id.country_description)
    TextView countryDescription;

    @Inject
    CountryDetailsPresenter presenter;
    @Inject
    Picasso picasso;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_details);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        compositeDisposable = new CompositeDisposable(
                presenter.getCountryItemObservable()
                        .subscribe(
                                new Consumer<CountryItem>() {
                                    @Override
                                    public void accept(CountryItem countryItem) throws Exception {
                                        picasso.load(countryItem.getImageUrl())
                                                .fit()
                                                .centerCrop()
                                                .placeholder(R.drawable.placeholder_big)
                                                .into(countryAvatar);

                                        countryName.setText(countryItem.getName());
                                        countryDescription.setText(countryItem.getDescription());
                                        countryPeopleAmount.setText(
                                                String.format("%s %s",
                                                        String.valueOf(countryItem.getPeopleAmount()),
                                                        getString(R.string.label_people)));
                                    }
                                }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void inject(Bundle savedInstanceState, AppComponent appComponent) {
        final Component component = DaggerCountryDetailsActivity_Component.builder()
                .appComponent(appComponent)
                .activityModule(new ActivityModule(this))
                .module(new Module(this))
                .build();
        component.inject(this);
    }

    @ActivityScope
    @dagger.Component(
            dependencies = AppComponent.class,
            modules = {
                    ActivityModule.class,
                    Module.class
            }
    )
    interface Component {
        void inject(CountryDetailsActivity activity);
    }

    @dagger.Module
    class Module {

        private final CountryDetailsActivity countryDetailsActivity;

        public Module(CountryDetailsActivity countryDetailsActivity) {
            this.countryDetailsActivity = countryDetailsActivity;
        }

        @Provides
        @Named("country_item")
        public CountryItem provideCountryItem() {
            return (CountryItem) getIntent().getExtras().getSerializable(EXTRA_COUNTRY_ITEM);
        }

        @Provides
        public CountryDetailsPresenter provideCountryDetailsPresenter(final @Named("country_item") CountryItem countryItem) {
            return new CountryDetailsPresenter(countryItem);
        }

    }
}
