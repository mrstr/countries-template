package com.example.rostrzep.countriesinterview.countires.country_details;

import com.example.rostrzep.countriesinterview.model.CountryItem;

import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;


public class CountryDetailsPresenter {

    private final Observable<CountryItem> countryItemObservable;

    @Inject
    public CountryDetailsPresenter(final @Named("country_item") CountryItem countryItem) {

        countryItemObservable = Observable.defer(
                new Callable<ObservableSource<? extends CountryItem>>() {
                    @Override
                    public ObservableSource<? extends CountryItem> call() throws Exception {
                        return Observable.just(countryItem);
                    }
                });
    }

    public Observable<CountryItem> getCountryItemObservable() {
        return countryItemObservable;
    }
}
