package com.example.rostrzep.countriesinterview.dagger;

import android.content.Context;

import com.example.rostrzep.countriesinterview.MainApplication;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

@Singleton
@Component(
        modules = {
                AppModule.class,
                SchedulersModule.class
        }

)
public interface AppComponent {

    void inject(MainApplication app);

    @ForApplication
    Context getContext();

    Picasso picasso();

    @UiScheduler
    Scheduler uiScheduler();

    @NetworkScheduler
    Scheduler networkScheduler();

    Gson gson();

}