package com.example.rostrzep.countriesinterview.model;


import java.util.List;

public class CountriesResponse {

    private final List<CountryItem> countryItemList;

    public CountriesResponse(List<CountryItem> countryItemList) {
        this.countryItemList = countryItemList;
    }

    public List<CountryItem> getCountryItemList() {
        return countryItemList;
    }
}
