package com.example.rostrzep.countriesinterview.model;

import java.io.Serializable;

public class CountryItem implements Serializable {

    private final String name;
    private final Integer imageUrl;
    private final Integer thumbImageUrl;
    private final String description;
    private final int peopleAmount;

    public CountryItem(String name, Integer imageUrl, Integer thumbImageUrl, String description, int peopleAmount) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.thumbImageUrl = thumbImageUrl;
        this.description = description;
        this.peopleAmount = peopleAmount;
    }


    public String getName() {
        return name;
    }

    public Integer getImageUrl() {
        return imageUrl;
    }

    public Integer getThumbImageUrl() {
        return thumbImageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPeopleAmount() {
        return peopleAmount;
    }
}
