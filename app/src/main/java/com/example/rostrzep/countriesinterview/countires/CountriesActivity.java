package com.example.rostrzep.countriesinterview.countires;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rostrzep.countriesinterview.BaseActivity;
import com.example.rostrzep.countriesinterview.R;
import com.example.rostrzep.countriesinterview.api.CountriesService;
import com.example.rostrzep.countriesinterview.countires.country_details.CountryDetailsActivity;
import com.example.rostrzep.countriesinterview.dagger.ActivityModule;
import com.example.rostrzep.countriesinterview.dagger.ActivityScope;
import com.example.rostrzep.countriesinterview.dagger.AppComponent;
import com.example.rostrzep.countriesinterview.dagger.NetworkScheduler;
import com.example.rostrzep.countriesinterview.dagger.UiScheduler;
import com.example.rostrzep.countriesinterview.model.CountriesResponse;
import com.example.rostrzep.countriesinterview.model.CountryItem;
import com.jakewharton.rxbinding2.view.RxView;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class CountriesActivity extends BaseActivity {


    @BindView(R.id.recycler_view_country_list)
    RecyclerView recyclerView;
    @BindView(R.id.progress_view)
    ProgressBar progressView;
    @BindView(R.id.empty_results_view)
    View emptyResultsView;

    @Inject
    CountryItemAdapter countryItemAdapter;

    private CountriesPresenter presenter;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(countryItemAdapter);

        compositeDisposable = new CompositeDisposable(

                presenter.getCountriesList()
                        .subscribe(new Consumer<CountriesResponse>() {
                            @Override
                            public void accept(CountriesResponse countriesResponse) throws Exception {
                                countryItemAdapter.bindData(countriesResponse.getCountryItemList());

                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(CountriesActivity.this, R.string.error_message, Toast.LENGTH_LONG).show();
                            }
                        }),
                presenter.getEmptyResultsObservable()
                        .subscribe(RxView.visibility(emptyResultsView)),
                presenter.getProgressObservable()
                        .subscribe(RxView.visibility(progressView)),
                presenter.countryItemClickObservable()
                        .subscribe(new Consumer<CountryItem>() {
                                       @Override
                                       public void accept(CountryItem countryItem) throws Exception {
                                           startActivity(CountryDetailsActivity.newIntent(CountriesActivity.this, countryItem));
                                       }
                                   }
                        ));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }


    @Override
    public void inject(Bundle savedInstanceState, AppComponent appComponent) {
        final Component component = DaggerCountriesActivity_Component.builder()
                .appComponent(appComponent)
                .activityModule(new ActivityModule(this))
                .module(new Module(this))
                .build();
        component.inject(this);
    }

    @ActivityScope
    @dagger.Component(
            dependencies = AppComponent.class,
            modules = {
                    ActivityModule.class,
                    Module.class
            }
    )
    interface Component {
        void inject(CountriesActivity activity);

        CountryItemAdapter.OnCountryItemClickListener provideOnCountryItemClickListener();

        CountriesService countriesServcie();
    }

    @dagger.Module
    class Module {

        private final CountriesActivity countriesActivity;

        public Module(final CountriesActivity countriesActivity) {
            this.countriesActivity = countriesActivity;
        }

        @Provides
        CountriesPresenter provideCountriesPresenter(final CountriesService countriesService,
                                                     @UiScheduler Scheduler uiScheduler,
                                                     @NetworkScheduler Scheduler networkScheduler) {
            presenter = new CountriesPresenter(countriesService, uiScheduler, networkScheduler);
            return presenter;
        }

        @Provides
        public CountryItemAdapter.OnCountryItemClickListener provideOnCountryItemClickListener(final CountriesPresenter countriesPresenter) {
            return countriesPresenter;
        }

        @Provides
        public CountryItemAdapter provideCountryItemAdapter(final LayoutInflater LayoutInflater,
                                                            final Picasso picasso,
                                                            final CountryItemAdapter.OnCountryItemClickListener onCountryItemClickListener) {
            return new CountryItemAdapter(LayoutInflater, picasso, onCountryItemClickListener);
        }

    }
}
