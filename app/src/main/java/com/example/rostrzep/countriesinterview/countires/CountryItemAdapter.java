package com.example.rostrzep.countriesinterview.countires;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rostrzep.countriesinterview.R;
import com.example.rostrzep.countriesinterview.model.CountryItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryItemAdapter extends RecyclerView.Adapter<CountryItemAdapter.CountriesViewHolder> {

    public interface OnCountryItemClickListener {
        void countryItemClicked(final CountryItem countryItem);
    }

    class CountriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_name)
        TextView countryName;
        @BindView(R.id.country_avatar)
        ImageView countryAvatar;
        @BindView(R.id.container_country_item)
        View countryContainer;
        @BindView(R.id.country_people_amount)
        TextView countryPeopleAmount;

        public CountriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final CountryItem countryItem) {

            if (countryItem.getThumbImageUrl() != null) {
                picasso.load(countryItem.getThumbImageUrl())
                        .placeholder(R.drawable.placeholder_small)
                        .fit()
                        .centerCrop()
                        .into(countryAvatar);
            } else {
                picasso.load(R.drawable.placeholder_small)
                        .fit()
                        .centerCrop()
                        .into(countryAvatar);
            }


            countryName.setText(countryItem.getName());
            countryPeopleAmount.setText(
                    String.format("%s %s",
                            String.valueOf(countryItem.getPeopleAmount()),
                            layoutInflater.getContext().getString(R.string.label_people)));

            countryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onCountryItemClickListener != null) {
                        onCountryItemClickListener.countryItemClicked(countryItem);
                    }
                }
            });
        }

    }

    @Override
    public CountriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountriesViewHolder(layoutInflater.inflate(R.layout.item_country, parent, false));
    }

    @Override
    public void onBindViewHolder(CountriesViewHolder holder, int position) {
        holder.bind(countryItems.get(position));
    }

    @Override
    public int getItemCount() {
        return countryItems == null ? 0 : countryItems.size();
    }

    public void bindData(final List<CountryItem> countryItems) {
        this.countryItems = countryItems;
        notifyDataSetChanged();
    }

    private final LayoutInflater layoutInflater;
    private final Picasso picasso;
    private final OnCountryItemClickListener onCountryItemClickListener;

    private List<CountryItem> countryItems = new ArrayList<>();

    @Inject
    public CountryItemAdapter(
            final LayoutInflater layoutInflater,
            final Picasso picasso,
            final OnCountryItemClickListener onCountryItemClickListener) {

        this.layoutInflater = layoutInflater;
        this.picasso = picasso;
        this.onCountryItemClickListener = onCountryItemClickListener;
    }
}
