package com.example.rostrzep.countriesinterview.dagger;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.rostrzep.countriesinterview.BuildConfig;
import com.example.rostrzep.countriesinterview.MainApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public final class AppModule {

    private final MainApplication application;

    public AppModule(MainApplication application) {
        this.application = application;
    }

    @Provides
    @ForApplication
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    static Picasso providePicasso(@ForApplication Context context) {
        final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        return new Picasso.Builder(context)
                .indicatorsEnabled(BuildConfig.DEBUG)
                .loggingEnabled(BuildConfig.DEBUG)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        if (BuildConfig.DEBUG) {
                            Log.e("picasso", uri.toString(), exception);
                        }
                    }
                })
                .downloader(new OkHttp3Downloader(okHttpClient.build()))
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }
}
