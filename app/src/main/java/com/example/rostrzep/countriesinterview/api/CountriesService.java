package com.example.rostrzep.countriesinterview.api;


import com.example.rostrzep.countriesinterview.R;
import com.example.rostrzep.countriesinterview.model.CountriesResponse;
import com.example.rostrzep.countriesinterview.model.CountryItem;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class CountriesService {

    private final Gson gson;

    @Inject
    public CountriesService(final Gson gson) {
        this.gson = gson;
    }

    public CountriesResponse getCountries() {
        final List<CountryItem> countryItemList = new ArrayList<>();

        countryItemList.add(new CountryItem("Brasil", R.drawable.bra, R.drawable.bra_thumb, "Some Description About Brasil", 150));
        countryItemList.add(new CountryItem("Switzerland", R.drawable.sui, R.drawable.sui_thumb, "Some Description About Brasil", 7));
        countryItemList.add(new CountryItem("Germany", R.drawable.ger, R.drawable.ger_thumb, "Some Description About Brasil", 88));
        countryItemList.add(new CountryItem("Italy", R.drawable.ita, R.drawable.ita_thumb, "Some Description About Brasil", 96));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        countryItemList.add(new CountryItem("France", R.drawable.fra, R.drawable.fra_thumb, "Some Description About Brasil", 99));
        return new CountriesResponse(countryItemList);
    }

}
