package com.example.rostrzep.countriesinterview;

import android.app.Application;

import com.example.rostrzep.countriesinterview.dagger.AppComponent;
import com.example.rostrzep.countriesinterview.dagger.AppModule;
import com.example.rostrzep.countriesinterview.dagger.DaggerAppComponent;


public class MainApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
    }

    private void setupGraph() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}