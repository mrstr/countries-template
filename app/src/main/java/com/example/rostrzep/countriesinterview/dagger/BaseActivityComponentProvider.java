package com.example.rostrzep.countriesinterview.dagger;

import android.os.Bundle;

public interface BaseActivityComponentProvider {

    void inject(Bundle savedInstanceState, AppComponent appComponent);

}
