package com.example.rostrzep.countriesinterview.countires;

import android.support.annotation.NonNull;

import com.example.rostrzep.countriesinterview.api.CountriesService;
import com.example.rostrzep.countriesinterview.dagger.NetworkScheduler;
import com.example.rostrzep.countriesinterview.dagger.UiScheduler;
import com.example.rostrzep.countriesinterview.model.CountriesResponse;
import com.example.rostrzep.countriesinterview.model.CountryItem;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;

public class CountriesPresenter implements CountryItemAdapter.OnCountryItemClickListener {

    private final Observable<Boolean> emptyResultsObservable;
    private final Observable<CountriesResponse> countriesList;

    private final Observable<Boolean> progressObservable;
    private final PublishSubject<CountryItem> countryClickSubject = PublishSubject.create();

    @Inject
    public CountriesPresenter(
            @NonNull final CountriesService countriesService,
            @NonNull @UiScheduler final Scheduler uiScheduler,
            @NonNull @NetworkScheduler final Scheduler networkScheduler) {

        countriesList = Observable.just(countriesService.getCountries())
                .subscribeOn(networkScheduler)
                .observeOn(uiScheduler);

        emptyResultsObservable = countriesList
                .map(new Function<CountriesResponse, Boolean>() {
                    @Override
                    public Boolean apply(CountriesResponse countriesResponse) throws Exception {
                        return !(countriesResponse != null &&
                                countriesResponse.getCountryItemList() != null) ||
                                countriesResponse.getCountryItemList().isEmpty();
                    }
                });

        progressObservable =
                Observable.merge(
                        Observable.just(true),
                        countriesList
                                .map(new Function<CountriesResponse, Boolean>() {
                                    @Override
                                    public Boolean apply(CountriesResponse countriesResponse) throws Exception {
                                        return false;
                                    }
                                })
                                .delay(5, TimeUnit.SECONDS, uiScheduler)); // Just to simulate network call
    }


    public Observable<Boolean> getEmptyResultsObservable() {
        return emptyResultsObservable;
    }

    public Observable<CountriesResponse> getCountriesList() {
        return countriesList;
    }

    public Observable<Boolean> getProgressObservable() {
        return progressObservable;
    }

    public Observable<CountryItem> countryItemClickObservable() {
        return countryClickSubject;
    }

    @Override
    public void countryItemClicked(final CountryItem countryItem) {
        countryClickSubject.onNext(countryItem);
    }
}
